const btnCalcular = document.getElementById('btnCalcular');

btnCalcular.addEventListener("click", function(){
//obtener valores de los inputs texts

let valorAuto = document.getElementById('valorAuto').value;
let pInicial = document.getElementById('porPagoInicial').value;
let plazos = document.getElementById('plazos').value;

//hacer los calculos

let pagoInicial = valorAuto * (pInicial/100);
let totalFin = valorAuto-pagoInicial;
let pagoMensual = totalFin/plazos;

//Mostrar los resultados

document.getElementById('pagoInicial').value = pagoInicial;
document.getElementById('totalFin').value = totalFin;
document.getElementById('pagoMensual').value = pagoMensual;

})